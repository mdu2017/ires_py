# Author: Mark Du
# Assignment: IRES Assignment
# Assignment Description: Take in a command line argument
#  as a directory and scan through all subdirectories and generate
#  AST files for source code files.
# Date: 12/12/2019

import sys
import os
import ast
import astpretty
import TreeVisitor

startDirectory = sys.argv[1]
ast_extension = ".ast"
py_extension = ".py"

src_files = []


# Recursively scan al subdirectories
def checkDirectory(start_directory):
    # file_name is a directory entry object
    for filepath in os.scandir(start_directory):
        if filepath.is_dir():
            checkDirectory(filepath.path)  # recursively scan subdirectories

        if filepath.name.endswith(py_extension):  # add src file to list
            src_files.append(filepath.path)


# Create the corresponding .ast file for each src file
def create_ast_files(source_files):
    for path in source_files:
        ast_file = path + ast_extension
        # print(ast_file)

        srcFile = open(str(path), 'r')
        srcCode = srcFile.read()
        srcFile.close()

        # Parse source file for AST
        rootNode = ast.parse(srcCode)
        astContent = astpretty.pformat(rootNode)

        # Tree visitor to visit various types of ast nodes
        # tree_vis = TreeVisitor.TreeVisitor()
        # print(str(path))
        # tree_vis.visit(rootNode)
        # print("\n=============================\n")

        file = open(str(ast_file), 'w')
        file.write(astContent)
        file.close()


# Checks for all .py files
checkDirectory(startDirectory)

# Create AST files
create_ast_files(src_files)
