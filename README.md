# README #

This contains instructions for running the application.

The application will scan through a given directory and all its 
subdirectories and generate an AST and store it in a .ast file 
for all source code files.

### Getting set up ####
* Clone repository to desired location
* Navigate to project folder
* Check to make sure Python 3.0 or above is used.
* Run the command (this will download any required libraries):
	- pip install -r requirements.txt

### Running the application ###
* How to run the program
	- From the project folder run the command:
	- - python main.py <starting_directory>
* How to run JUnit tests
	- Navigate to project folder root directory and run:
	- - python -m unittest discover -s ./test
	
### Information ###
* Environment
	- Jetbrains PyCharm
* Configuration
	- Python 3.7.4
* Libraries
	- ast
	- sys
	- os
	- astpretty

### Contact ###

* mark_du1@baylor.edu


