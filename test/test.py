# Author: Mark Du
# Assignment: IRES Assignment
# Assignment Description: Take in a command line argument
#  as a directory and scan through all subdirectories and generate
#  AST files for source code files.
# Date: 12/12/2019

import os
import unittest
import ast

sampleFile = "test/Unit.py"


class TestMethods(unittest.TestCase):
    def testFile(self):
        fileList = []
        for filepath in os.scandir("."):
            if filepath.name.endswith(".py"):  # add src file to list
                fileList.append(filepath.path)

        self.assertTrue(fileList)  # Checks if list is not empty

    def testAST(self):
        testFile = open(sampleFile, "r")
        testCode = testFile.read()
        testFile.close()

        testNode = ast.parse(testCode)
        astStr = ast.dump(testNode)

        self.assertTrue(astStr)

    def testContains(self):
        testFile = open(sampleFile, "r")
        testCode = testFile.read()
        testFile.close()

        testNode = ast.parse(testCode)
        astStr = ast.dump(testNode)

        test_msg = "Testing Variable"

        self.assertTrue(astStr.find(test_msg))
