# File for AST Tree visitor
import ast


# Visitor for the different types of nodes in AST
class TreeVisitor(ast.NodeVisitor):
    def visit_Num(self, node):
        print("Number node: ", node._fields)
        self.generic_visit(node)

    def visit_Str(self, node):
        print("String node: ", node._fields)
        self.generic_visit(node)

    def visit_Assign(self, node):
        print("Assignment node: ", node._fields)
        self.generic_visit(node)

    def visit_Import(self, node):
        print("Import nodes: ", node._fields)
        self.generic_visit(node)

    def visit_Expr(self, node):
        print("Expression nodes: ", node._fields)
        self.generic_visit(node)
