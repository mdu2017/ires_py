# Sample file for generating an AST

x = 10
y = 20
z = 15

if x > y:
    print("x > y")
elif x > z:
    print("x > z")
else:
    print("x < y and x < z")
